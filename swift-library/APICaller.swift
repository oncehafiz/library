//
//  APICaller.swift
//  Circumspex
//
//  Created by Hafiz Haseeb
//  Copyright (c) 2015 Ching. All rights reserved.
//

import Foundation
import UIKit

class APICaller: NSObject, NSURLConnectionDelegate
{
    
    
    let WebAPIURL = "http://"
    let ConnectHead = "No connection found"
    let ConnectDes  = "Kindly check your connection"
    let AuthHeadDel = "Authentication"
    let AuthHeadSuccess = "Success"
    let AuthProvider = "Kindly select service provider"
    let AuthHeadProvider = "Warning"
    let IsServiceProviderAvailable = "Service provider not available on phone"
    var responseData:NSData? = nil
    
    
    func sendPostCall(params : AnyObject, url : String, postCompleted : (succeeded: Bool, result: AnyObject) -> ()) {
        
        
        let request = NSMutableURLRequest(URL: NSURL(string: WebAPIURL + url)!)
        request.HTTPMethod = "POST"
        request.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            // let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//            print(responseData)
//            print()
            let responseResult = self.parseDataToJSON(data!)
            
            if responseResult == nil {
                
                postCompleted(succeeded: false, result: "Error")
                
            }else{
                
                postCompleted(succeeded: true, result: responseResult!)
                print(responseResult)
            }
            
        }
        task.resume()
    }
    
    
    
    
    func sendGetCall( url : String, postCompleted : (succeeded: Bool, result: AnyObject) -> ()) {
        let request = NSMutableURLRequest(URL: NSURL(string: WebAPIURL + url)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        
        var err: NSError?
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            print("Response: \(response)")
            
            // Print reponse in String on console
            let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Body: \(strData)")
            
            let responseResult = self.parseDataToJSON(data!)
            
            if responseResult == nil {
                
                postCompleted(succeeded: false, result: "Error")
                
            }else{
                
                postCompleted(succeeded: true, result: responseResult!)
            }
            
        })
        
        task.resume()
    }
    
    
    
    
    func parseDataToJSON(data:NSData) -> NSDictionary? {
        
        var err: NSError?
        var json: NSDictionary?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
        } catch let error as NSError {
            err = error
        }
        
        
        
        // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
        if(err != nil) {
            print(err!.localizedDescription)
            let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
            print("Error could not parse JSON: '\(jsonStr)'")
            return nil
        }
        else {
            // The JSONObjectWithData constructor didn't return an error. But, we should still
            // check and make sure that json has a value using optional binding.
            if let parseJSON = json {
                // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                if let success = parseJSON["error"] as? Bool {
                    print("Error: \(success)")
                    
                }
                
                return json
            }
            else {
                // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: \(jsonStr)")
                return nil
            }
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]! {
        print(text)
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func convertImageToBase64(image:UIImage) -> String {
        
        // let image:UIImage = UIImage(named: "about_back-button.png")!
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        return base64String!
    }
    
}
