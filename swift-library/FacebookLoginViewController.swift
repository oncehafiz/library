//
//  FacebookLoginViewController.swift
//  swift-library
//
//  Created by Hafiz on 9/2/16.
//  Copyright © 2016 Hafiz. All rights reserved.
//

// MARK: Import all library

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import MapKit
import CoreLocation
import SimpleAlert

class FacebookLoginViewController: UIViewController,FBSDKLoginButtonDelegate , UIAlertViewDelegate , UITextFieldDelegate, CLLocationManagerDelegate {
    
    // MARK: Define properties
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var SignupEmail: UIButton!
    
   
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
            return UIStatusBarStyle.LightContent
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    var window: UIWindow?
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        if( error != nil)
        {
            print(error.localizedDescription)
            return
        }
        
        if let userToken = result.token{
            
            dispatch_async(dispatch_get_main_queue()) {
                self.blurView.hidden = false
                self.activityIndicatorView.getViewActivityIndicator().hidden = false
            }
            
            
            //Get user acces token
            let token:FBSDKAccessToken=result.token
            
            print("Token = \(FBSDKAccessToken.currentAccessToken().tokenString)")
            
            print("User ID = \(FBSDKAccessToken.currentAccessToken().userID)")
            
            
            
            //Show user information
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,name,cover,first_name,last_name,email,picture.type(large),link,birthday, bio ,location ,friends ,hometown , friendlists,photos"])
            
            graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                
                if ((error) != nil)
                {
                    // Process error
                    print("Error: \(error)")
                }
                else
                {
                    
                    
                    
                    let prefs1:NSUserDefaults  = NSUserDefaults.standardUserDefaults()
                    let tokenUUId = prefs1.objectForKey("Token") as! NSData
                    
                    let resstr = tokenUUId.hexString()
                    
                    let tokenString = resstr as! String
                    
                    
                    print("fetched user: \(result)")
                    let email = result.valueForKey("email")
                    if(email != nil )
                    {
                        let firstName : NSString = result.valueForKey("first_name") as! NSString
                        let lastName : NSString = result.valueForKey("last_name") as! NSString
                        let fbid = result.valueForKey("id")
                        let picture = result.valueForKey("picture")
                        let cover = result.valueForKey("cover")
                        
                        
                        let data = picture!.valueForKey("data")
                        let urlPhoto = data!.valueForKey("url")
                        let sourceCover = cover!.valueForKey("source")
                        
                        
                        let postString: [String:AnyObject] = [
                            "isProvider" : prefs.stringForKey("prefs")!,
                            "firstName"  : firstName,
                            "lastName"   : lastName,
                            "fbId"       : fbid!,
                            "fbToken"    : FBSDKAccessToken.currentAccessToken().tokenString,
                            "fbLink"     : result.valueForKey("link")!,
                            "photoPath"  : urlPhoto!, //self.returnUserProfileImage(FBSDKAccessToken.currentAccessToken().tokenString),
                            "email"      : email!,
                            "coverPhoto"  : sourceCover!
                        ]
                        
                        if(Reachability.isConnectedToNetwork()){
                            
                            
                            let cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
                            let request = NSMutableURLRequest(URL: url!, cachePolicy: cachePolicy, timeoutInterval: 10.0)
                            request.HTTPMethod = "POST"
                            
                            do{
                                let theJSONData = try NSJSONSerialization.dataWithJSONObject(postString , options: NSJSONWritingOptions(rawValue: 0))
                                request.HTTPBody = theJSONData
                                request.setValue("text/plain", forHTTPHeaderField: "Content-Type")
                                request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
                                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                                let theJSONText = NSString(data: theJSONData,
                                    encoding: NSASCIIStringEncoding)!
                                
                                
                                print("\n\(theJSONText)")
                            }
                            catch
                            {
                                print("error")
                            }
                            
                            
                            
                            var response: NSURLResponse? = nil
                            do {
                                let reply: NSData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
                                
                                if let results = NSString(data:reply, encoding:NSUTF8StringEncoding) {
                                    
                                    var isValidJson = apiCallers.convertStringToDictionary(results as String)
                                    var resultResponse = isValidJson["result"]!
                                    
                                    let status = resultResponse.valueForKey("status") as! NSString
                                    let statusResponse = resultResponse.valueForKey("response") as! NSString
                                    
                                    // var value  =
                                    if( status == "error"){
                                        dispatch_async(dispatch_get_main_queue(), {
                                            SCLAlertView().showInfo(apiCallers.AuthHeadDel, subTitle: statusResponse as String)
                                        })
                                        
                                        dispatch_async(dispatch_get_main_queue()) {
                                            self.blurView.hidden = true
                                            self.activityIndicatorView.getViewActivityIndicator().hidden = true
                                        }
                                       
                                        
                                        
                                        
                                        
                                        
                                    }
                                    else{
                                        
                                        
                                        
                                        var userDataResponse = isValidJson["user_data"]!
                                        
                                        
                                        
                                        
                                        self.view.frame.origin.y = 0
                                        
                                        
                                        
                                        dispatch_async(dispatch_get_main_queue()) {
                                            
                                            self.blurView.hidden = false
                                            self.activityIndicatorView.getViewActivityIndicator().hidden = false
                                            
                                            
                                        }
                                        
                                        
                                        
                                    }
                                    
                                } else {
                                    print("Nill Here")
                                }
                                
                                
                            } catch {
                                print("Error in response")
                            }
                        }
                        
                        //callLocalDBEvent(results)
                    }
                        
                        
                        
                        
                        
                    else {
                        dispatch_async(dispatch_get_main_queue(), {
                            SCLAlertView().showInfo(apiCallers.ConnectHead, subTitle: apiCallers.ConnectDes)
                        })
                    }
                    
                }
            })
            
            
            
        }
        
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    // MARK: Return user facebook profile image
    
    func returnUserProfileImage(accessToken: NSString) -> String
    {
        var userID = accessToken as NSString
        var facebookProfileUrl = NSURL(string: "http://graph.facebook.com/\(userID)/picture?type=large")
        
        if let data = NSData(contentsOfURL: facebookProfileUrl!) {
            return String(facebookProfileUrl!)
        }
        return String(facebookProfileUrl!)
        
    }
    
    
    @IBOutlet weak var FBSignupBtn: FBSDKLoginButton!
    var push = true

    
    @IBAction func signupFB(sender: AnyObject) {
        
        self.FBSignupBtn.readPermissions = ["public_profile", "email", "user_friends"]
        self.FBSignupBtn.delegate = self
        blurView.hidden = false
        self.activityIndicatorView.getViewActivityIndicator().hidden = false
        
        
    }
    
    
    var activityIndicatorView: ActivityIndicatorView!
    var blurView:UIVisualEffectView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .None)
        self.activityIndicatorView = ActivityIndicatorView(title: "please wait...", center: self.view.center)
        blurView =  self.activityIndicatorView.blurView(view.bounds)
        view.addSubview(blurView)
        self.view.addSubview(self.activityIndicatorView.getViewActivityIndicator())
        blurView.hidden = true
        self.activityIndicatorView.getViewActivityIndicator().hidden = true
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        dispatch_async(dispatch_get_main_queue()) {
            self.blurView.hidden = true
            self.activityIndicatorView.getViewActivityIndicator().hidden = true
        }
    }
    
    
    
    // MARK: Convert image to string
    func convertImageToBase64(image:UIImage) -> String {
        
        // let image:UIImage = UIImage(named: "about_back-button.png")!
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        return base64String!
    }
    
    
}