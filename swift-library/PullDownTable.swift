//
//  PullDownTable.swift
//  swift-library
//
//  Created by Hafiz on 9/2/16.
//  Copyright © 2016 Hafiz. All rights reserved.
//  once.hafiz@gmail.com

import UIKit
import Foundation


func resizeImage(image:UIImage, toTheSize size:CGSize)->UIImage{
    let scale = CGFloat(max(size.width/image.size.width,
        size.height/image.size.height))
    let width:CGFloat  = image.size.width * scale
    let height:CGFloat = image.size.height * scale;
    
    let rr:CGRect = CGRectMake( 0, 0, width, height);
    
    UIGraphicsBeginImageContextWithOptions(size, false, 0);
    image.drawInRect(rr)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext();
    return newImage
}

var requestDictionaryProvider : [String : [String:AnyObject]] = Dictionary()
var requestDictionaryBegin : [String : AnyObject] = Dictionary()



class PullDownViewController: UIViewController , UITableViewDelegate, UITableViewDataSource , CLLocationManagerDelegate {
    
    
    
    var favouritiesArray : NSMutableArray = []
    var activityIndicatorView: ActivityIndicatorView!
    var blurView:UIVisualEffectView!
    
    var refreshControl = UIRefreshControl()
    
    
    var TableData:Array< datastruct > = Array < datastruct >()
    
    enum ErrorHandler:ErrorType
    {
        case ErrorFetchingResults
    }
    
    
    struct datastruct
    {
        var imageurl:String?
        var image:UIImage? = nil
        init(add: NSDictionary)
        {
            imageurl = add["url"] as? String
            
        }
    }
    
    let secondsare = 4.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicatorView = ActivityIndicatorView(title: "please wait...", center: self.view.center)
        blurView =  self.activityIndicatorView.blurView(view.bounds)
        view.addSubview(blurView)
        self.view.addSubview(self.activityIndicatorView.getViewActivityIndicator())
        blurView.hidden = false
        self.activityIndicatorView.getViewActivityIndicator().hidden = false
        let cateID = String(requestDictionaryCategory["categoryId"]!)
        let categoryId = Int(cateID)
        
        for (var catArray = 0; catArray < categoriesArray.count; catArray++)
        {
            let singleObjects = categoriesArray.objectAtIndex(catArray)
            if((singleObjects.objectForKey("categoryId")!) as! String == cateID)
            {
                favouritiesArray.addObject(singleObjects)
            }
            
            
        }
        
        
        
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    func updatePicture()
    {
        let myTimer : NSTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("updateTableView:"), userInfo: nil, repeats: false)
    }
    func updateTableView(sender:AnyObject)
    {
        self.whotableView.reloadData()
    }
    override func viewWillAppear(animated: Bool)
    {
        
        
        self.TableData.removeAll()
        
        dispatch_async(dispatch_get_main_queue()) {
            if(categoriesArray.count > 0){
                for (var i = 0 ; i < self.favouritiesArray.count ; i++)
                {
                    let singleObjects = self.favouritiesArray.objectAtIndex(i)
                    print(singleObjects)
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.blurView.hidden = true
                        self.activityIndicatorView.getViewActivityIndicator().hidden = true
                    }
                    
                }
                self.updatePicture()
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.blurView.hidden = true
                    self.activityIndicatorView.getViewActivityIndicator().hidden = true
                    self.whotableView.delegate = self
                    self.whotableView.dataSource = self
                    self.whotableView.reloadData()
                }
            }
            
            
            
        }
        
        
        
    }
    
    func reloadCurrentView()
    {
        
        
        print("===============Favourities Before====================")
        print(favouritiesArray.count)
        print("========================== ==========================")
        favouritiesArray.removeAllObjects()
        let cateID = String(requestDictionaryCategory["categoryId"]!)
        print(cateID)
        let categoryId = Int(cateID)
        
        
        
        
        self.TableData.removeAll()
        
        for (var catArray = 0; catArray < categoriesArray.count; catArray++)
        {
            let singleObjects = categoriesArray.objectAtIndex(catArray)
            if((singleObjects.objectForKey("categoryId")!) as! String == cateID)
            {
                favouritiesArray.addObject(singleObjects)
            }
        }
        
        print("===============Favourities After====================")
        print(favouritiesArray.count)
        print("========================== ==========================")
        
        dispatch_async(dispatch_get_main_queue()) {
            if(categoriesArray.count > 0){
                for (var i = 0 ; i < self.favouritiesArray.count ; i++)
                {
                    let singleObjects = self.favouritiesArray.objectAtIndex(i)
                    if self.refreshControl.refreshing
                    {
                        self.refreshControl.endRefreshing()
                    }
                    
                    self.whotableView.reloadData()
                    
                }
                self.updatePicture()
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.blurView.hidden = true
                    self.activityIndicatorView.getViewActivityIndicator().hidden = true
                    self.whotableView.delegate = self
                    self.whotableView.dataSource = self
                    self.whotableView.reloadData()
                }
            }
            
            
            
        }
        
        
    }
    
    func refresh(sender:AnyObject) {
        self.refreshControl.tintColor = UIColor.lightGrayColor()
        self.callReloadService()
    }
    
    
    func callReloadService()
    {
        if(Reachability.isConnectedToNetwork())
        {
            
            let urlByUser = NSURL(string: apiCallers.WebAPIURL + "/category/list?sessionId=" + sessionId.stringForKey("sessionId")!)
            let cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
            let request = NSMutableURLRequest(URL: urlByUser!, cachePolicy: cachePolicy, timeoutInterval: 10.0)
            
            request.HTTPMethod = "GET"
            
            var response: NSURLResponse? = nil
            do {
                let reply: NSData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
                if let results = NSString(data:reply, encoding:NSUTF8StringEncoding) {
                    
                    var isValidJson = apiCallers.convertStringToDictionary(results as String)
                    
                    
                    
                    // var value  =
                    
                    ParseArray(results)
                    
                    
                    
                }
                else
                {
                    print("Nill Here")
                }
                
                
                
                
            }
            catch
            {
                print("Error in response")
            }
            
            
        }
        self.reloadCurrentView()
    }
    
    
    
    func ParseArray(results:NSString)
    {
        var isValidJson = apiCallers.convertStringToDictionary(results as String)
        
        var resultResponse = isValidJson["result"]!
        print(resultResponse)
        let status = resultResponse.valueForKey("status") as! NSString
        let statusResponse = resultResponse.valueForKey("response") as! NSString
        // var value  =
        if( status == "error"){
            dispatch_async(dispatch_get_main_queue(), {
                SCLAlertView().showInfo(apiCaller.AuthHeadDel, subTitle: statusResponse as String)
            })
            
        } else {
            let providerlists = isValidJson["providers"]!
            if(providerlists.count > 0)
            {
                updateFavouritiesArray(providerlists as! NSMutableArray)
            }
        }
    }
    
    func updateFavouritiesArray(providerlists:NSMutableArray)
    {
        categoriesArray.removeAllObjects()
        for var start = 0; start < providerlists.count; start++
        {
            let singleObject = providerlists[start]
            categoriesArray.addObject(singleObject)
            
            //                let lat = (singleObject.objectForKey("lat")!) as! NSString
            //                let lon = (singleObject.objectForKey("lon")!) as! NSString
            
        }
    }
    
   
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: Who Table view number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceProviderLocationArray.count
    }
    
    // MARK: Who Table view select row at index path
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let getServiceProviderId = favouritiesArray.objectAtIndex(indexPath.row)
        
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor(red: 216/255.0, green: 238/255.0, blue: 251/255.0, alpha: 1.0)
        
        
        
        let next = self.storyboard?.instantiateViewControllerWithIdentifier("whodetail") as! WhoDetailViewController
                next.i = indexPath.row
        self.navigationController?.pushViewController(next, animated: true )
        
        
    }
    
    
    
    // MARK: Who Table view cells
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var cell: WhoCell! = tableView.dequeueReusableCellWithIdentifier("whoSP", forIndexPath: indexPath) as! WhoCell
        if (cell == nil) {
            print("1")
            cell = WhoCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "whoSP")
            //cell = tableViewCell
        }
        
        
        
        cell.selectedBackgroundView?.backgroundColor = UIColor(red: 216/255.0, green: 238/255.0, blue: 251/255.0, alpha: 1.0)
        
        cell.imageView?.layer.masksToBounds = true
        let cellImageLayer: CALayer?  = cell?.imageView!.layer
        cell.timeLabel.text = (String(format: "%.1f", distanceArray[indexPath.row])) + " KM"
        cellImageLayer!.cornerRadius = 23
        cellImageLayer!.masksToBounds = true
        var url = PhotopathArray[indexPath.row] as String
        let data = TableData[indexPath.row]
        
        if (data.image == nil)
        {
            var image =  UIImage(named:"user_icon_img.png")
            let newImage = resizeImage(image!, toTheSize: CGSizeMake(46, 46))
            cell.imageView?.image = newImage
            load_image(url, imageview: cell.imageView!, index: indexPath.row)
        }
        else
        {
            var image = TableData[indexPath.row].image
            let newImage = resizeImage(image!, toTheSize: CGSizeMake(46, 46))
            cell.imageView?.image = newImage
            
        }
        cell.spHeader.numberOfLines = 0
        cell.spHeader.adjustsFontSizeToFitWidth = false;
        cell.spHeader.lineBreakMode = .ByTruncatingTail;
        
        cell.spHeader.sizeToFit()
        cell.spHeader.text = businessNameArray[indexPath.row] as String
        cell.spLink.text = contactNameArray[indexPath.row] as String
        cell.separatorInset = UIEdgeInsetsZero
        // cell.spBtn.userInteractionEnabled = false
        cell.spBtn.tag = (SpIdArray[indexPath.row]).integerValue
        
        
        
        
        
        if(!requestArray.isEmpty)
        {
            if(requestArray.contains(SpIdArray[indexPath.row].integerValue))
            {
                cell.spBtn.setImage(UIImage(named: "next_btn.png"), forState: UIControlState.Normal)
                
            }
            else
            {
                cell.spBtn.setImage(UIImage(named: "light_gray_img.png"), forState: UIControlState.Normal)
                
            }
        }
        
        return cell
    }
    
    
    func load_image(urlString:String, imageview:UIImageView, index:NSInteger)
    {
        
        let url:NSURL = NSURL(string: urlString)!
        let session = NSURLSession.sharedSession()
        
        let task = session.downloadTaskWithURL(url) {
            (
            let location, let response, let error) in
            
            guard let _:NSURL = location, let _:NSURLResponse = response  where error == nil else {
                print("error")
                return
            }
            
            let imageData = NSData(contentsOfURL: location!)
            
            dispatch_async(dispatch_get_main_queue(), {
                var image = UIImage(data: imageData!)
                let newImage = resizeImage(image!, toTheSize: CGSizeMake(46, 46))
                self.TableData[index].image = newImage
                imageview.image = self.TableData[index].image
                return
            })
            
            
        }
        
        task.resume()
        
        
    }
    
   
    
}

