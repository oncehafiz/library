//
//  Swift-Bridging-Header.h
//  swift-library
//
//  Created by Hafiz on 9/2/16.
//  Copyright © 2016 Hafiz. All rights reserved.
//

#ifndef Swift_Bridging_Header_h
#define Swift_Bridging_Header_h
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Google/Analytics.h"

#endif /* Swift_Bridging_Header_h */
