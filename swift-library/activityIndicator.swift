//
//  activityIndicator.swift
//  vehiclelogbookgps
//
//  Created by PSSMac-001 on 11/02/2016.
//  Copyright © 2016 ProSoftSol. All rights reserved.
//

import UIKit
import Foundation

class ActivityIndicatorView
{
    var view: UIView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    var title: String!
    
    init(title: String, center: CGPoint, width: CGFloat = 165.0, height: CGFloat = 121)
    {
        self.title = title
        
        let x = center.x - width/2.0
        let y = center.y - height/2.0
        
        self.view = UIView(frame: CGRect(x: x, y: y, width: width, height: height))
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "loader_img_bg.png")!)
        // UIColor(red:59/255.0, green:59/255.0, blue:59/255.0, alpha:1.0)
        self.view.layer.cornerRadius = 10
        self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 32, y: 10, width: 100, height: 70))
        
        self.activityIndicator.color = UIColor.whiteColor()
        self.activityIndicator.hidesWhenStopped = false
        // self.activityIndicator.backgroundColor = UIColor(patternImage: UIImage(named: "loader_img.gif")!)
        
        let titleLabel = UILabel(frame: CGRect(x: 50, y: 50, width: 150, height: 50))
        titleLabel.text = title
        titleLabel.textColor = UIColor.whiteColor()
        
        let imageName = "loader_img.gif"
//        let image = UIImage.animatedImageNamed(imageName, duration: 10)
//        let imageView = UIImageView(image: image!)
//
//        imageView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        // self.view.addSubview(imageView)
        
        self.view.addSubview(self.activityIndicator)
        // self.view.addSubview(titleLabel)
    }
    
    func getViewActivityIndicator() -> UIView
    {
        self.activityIndicator.startAnimating()
        return self.view
    }
    
    func startAnimating()
    {
        var transform: CGAffineTransform = CGAffineTransformMakeScale(4.5, 4.5)
        self.activityIndicator.transform = transform
        self.activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopAnimating()
    {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        
        self.view.removeFromSuperview()
    }
    func blurView(size:CGRect) -> UIVisualEffectView
    {
    
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        var blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = size
        blurView.alpha = 0.6
        return blurView

    }
}

