//
//  reachability.swift
//  vehiclelogbookgps
//
//  Created by PSSMac-001 on 2016/01/08.
//  Copyright © 2016 ProSoftSol. All rights reserved.
//

import Foundation
public class Reachability {
    
    class func isConnectedToNetwork()->Bool{
        
        var Status:Bool = false
        let url = NSURL(string: "http://google.com/")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "HEAD"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        
        var response: NSURLResponse?
        do {
        var data = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response) as NSData?
        } catch
        {
            
        }
        
        if let httpResponse = response as? NSHTTPURLResponse {
            if httpResponse.statusCode == 200 {
                Status = true
            }
        }
        
        return Status
    }
}